# Weather Forecast Application
![alt text](src/assets/images/screen-app.jpg)

## General info
Weather forecast is exceptionally easy to use application for staying always updated with the weather conditions.  
The app includes:  
* Current weather data for any location including current date, wind, humidity, actual temperature, sunrise/sunset times;  
* Daily forecast for 6 days including date, wind, humidity, actual min/max temperatures, sunrise/sunset times.

## Features

* Weather supports geo-positioning, retrieving the latest weather conditions for your current location.  
* An option to manually add  location for search.  

App uses real data received from [OpenWeatherMap API](https://openweathermap.org/).

## Technologies
This application created with:
* HTML
* CSS
* JavaScript

### Go to application: [Weather Forecast App](https://javascript103.gitlab.io/weather-forecast-app/)

import '../styles/reset.css';
import '../styles/style.scss';
import { startApp } from './app';

const defaultCity = 'Kharkiv';
startApp(defaultCity);

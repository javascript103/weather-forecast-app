const axios = require('axios').default;

const weekdays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const months = [
  'January',
  'February',
  'March',
  '"April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const weatherIcons = {
  '01d': 'assets/icons/clear_sky_d.png',
  '01n': 'assets/icons/clear_sky_n.png',
  '02d': 'assets/icons/few_clouds_d.png',
  '02n': 'assets/icons/few_clouds_n.png',
  '03d': 'assets/icons/scattered_clouds_d.png',
  '03n': 'assets/icons/scattered_clouds_n.png',
  '04d': 'assets/icons/broken_clouds_d.png',
  '04n': 'assets/icons/broken_clouds_n.png',
  '09d': 'assets/icons/shower_rain_d.png',
  '09n': 'assets/icons/shower_rain_n.png',
  '10d': 'assets/icons/rain_d.png',
  '10n': 'assets/icons/rain_n.png',
  '11d': 'assets/icons/thunderstorm_d.png',
  '11n': 'assets/icons/thunderstorm_n.png',
  '13d': 'assets/icons/snow_d.png',
  '13n': 'assets/icons/snow_n.png',
  '50d': 'assets/icons/mist_d.png',
  '50n': 'assets/icons/mist_n.png',
};

const searchFormElement = document.querySelector('#search-form');
const searchInputElement = document.querySelector('#search-input');
const locationElement = document.querySelector('#search-location-button');

const newDateElement = document.querySelector('#date');
const newDateDayElement = document.querySelector('#date-day');
const cityElement = document.querySelector('#city');
const iconElement = document.querySelector('#icon');
const temperatureElement = document.querySelector('#temperature');
const celsiusLink = document.querySelector('#celsius');
const fahrenheitLink = document.querySelector('#fahrenheit');
const descriptionElement = document.querySelector('#description');
const windElement = document.querySelector('#wind');
const humidityElement = document.querySelector('#humidity');
const forecastElement = document.querySelector('#forecast');
const sunriseElement = document.querySelector('#sunrise');
const sunsetElement = document.querySelector('#sunset');

const apiKey = '1ad6e66daecab484d39c734aa3d7405a';
const baseUrl = `https://api.openweathermap.org/data/2.5/weather?appid=${apiKey}&units=metric`;

let state = {
  currentUnit: 'C',
  celsiusTemperature: 0,
};

function formatDate() {
  const currentDate = new Date();
  const weekday = weekdays[currentDate.getDay()];
  const date = currentDate.getDate();
  const month = months[currentDate.getMonth()];
  const year = currentDate.getFullYear();
  let hour = currentDate.getHours();
  if (hour < 10) {
    hour = `0${hour}`;
  }
  let minutes = currentDate.getMinutes();
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  newDateDayElement.innerHTML = weekday;
  newDateElement.innerHTML = `${date} ${month} ${year}, ${hour}:${minutes}`;
}

function formatDayForecast(timestamp) {
  const dateForecast = new Date(timestamp * 1000);
  const dayForecast = dateForecast.getDay();
  return weekdays[dayForecast];
}

function formatDateForecast(timestamp) {
  const dateForecast = new Date(timestamp * 1000);
  const dateOfMonthForecast = dateForecast.getDate();
  const monthForecast = months[dateForecast.getMonth()];
  const yearForecast = dateForecast.getFullYear();
  return `${dateOfMonthForecast} ${monthForecast} ${yearForecast}`;
}

function formatTimeForecast(timestamp) {
  const dateForecast = new Date(timestamp * 1000);
  let hourForecast = dateForecast.getHours();
  if (hourForecast < 10) {
    hourForecast = `0${hourForecast}`;
  }
  let minutesForecast = dateForecast.getMinutes();
  if (minutesForecast < 10) {
    minutesForecast = `0${minutesForecast}`;
  }
  return `${hourForecast}:${minutesForecast}`;
}

function forecastDisplay(response) {
  const forecastDays = response.data.daily;
  let forecastHTML = '';
  forecastDays.forEach((forecastDay, index) => {
    if (index < 7 && index !== 0) {
      forecastHTML += `
        <div class="secondary-item-wrapper">
          <div class="secondary-item">
            <div class='secondary-item__weekday'>${formatDayForecast(forecastDay.dt)}</div>
            <div class='secondary-item__data'>${formatDateForecast(forecastDay.dt)}</div>
            <div class='secondary-item__icon'>
              <img 
              src='${weatherIcons[forecastDay.weather[0].icon]}' 
              alt='${forecastDay.weather[0].description}' 
              />
            </div>
            <div class='secondary-item__temperature'>${Math.round(forecastDay.temp.min)}&harr;${Math.round(forecastDay.temp.max)}°C</div>
            <div class='secondary-item__description'>${forecastDay.weather[0].main}</div>
            </div>
          <div class='secondary-item__card'>
            <img
              class='secondary-item__card-icon'
              src='assets/icons/sunrise.svg'
              alt='sunrise'
            />
            <p class='secondary-item__card-name'>Sunrise</p>
            <p class='secondary-item__card-value'>${formatTimeForecast(forecastDay.sunrise)}<span> AM</span></p>
          </div>
          <div class='secondary-item__card'>
            <img
              class='secondary-item__card-icon'
              src='assets/icons/sunset.svg'
              alt='sunset'
            />
            <p class='secondary-item__card-name'>Sunset</p>
            <p class='secondary-item__card-value'>${formatTimeForecast(forecastDay.sunset)}<span> PM</span></p>
          </div>
        </div>
      `;
    }
  });
  forecastElement.innerHTML = forecastHTML;
}

function getForecast(coordinates) {
  const apiUrl = `https://api.openweathermap.org/data/2.5/onecall?lat=${coordinates.lat}&lon=${coordinates.lon}&appid=${apiKey}&units=metric`;
  axios.get(apiUrl).then(forecastDisplay);
}

function getFormattedTemp(temperature) {
  if (state.currentUnit === 'C') {
    return Math.round(temperature);
  }
  return Math.round((temperature * 9) / 5 + 32);
}

function updateState(response) {
  state = {
    ...state,
    celsiusTemperature: response.data.main.temp,
  };
}

function setCityName(city) {
  cityElement.innerHTML = city.trim();
}

function showData(response) {
  setCityName(response.data.name);
  temperatureElement.innerHTML = getFormattedTemp(state.celsiusTemperature, state.currentUnit);
  iconElement.setAttribute('src', weatherIcons[response.data.weather[0].icon]);
  iconElement.setAttribute('alt', response.data.weather[0].description);
  descriptionElement.innerHTML = response.data.weather[0].main;
  humidityElement.innerHTML = `Humidity: ${response.data.main.humidity} %`;
  windElement.innerHTML = `Wind: ${response.data.wind.speed} km/h`;
  sunriseElement.innerHTML = `Sunrise: ${formatTimeForecast(response.data.sys.sunrise)} AM`;
  sunsetElement.innerHTML = `Sunset: ${formatTimeForecast(response.data.sys.sunset)} PM`;

  getForecast(response.data.coord);
}
function showErrorMassage() {
  cityElement.innerHTML = 'City is not found';
}

function getWeatherByCityName(cityName) {
  const url = `${baseUrl}&q=${cityName}`;
  axios.get(url).then((response) => {
    updateState(response);
    showData(response);
  }).catch(() => {
    showErrorMassage();
  });
}

function searchCity(event) {
  event.preventDefault();
  if (searchInputElement.value.length > 0) {
    getWeatherByCityName(searchInputElement.value);
    formatDate();
    searchFormElement.reset();
  }
}

function showPosition(position) {
  const lat = position.coords.latitude;
  const lon = position.coords.longitude;
  const url = `${baseUrl}&lat=${lat}&lon=${lon}`;
  axios.get(url).then((response) => {
    showData(response);
    setCityName(response.data.name);
  });
}

function getCurrentPosition() {
  navigator.geolocation.getCurrentPosition(showPosition);
}

function showCelsius() {
  state.currentUnit = 'C';
  temperatureElement.innerHTML = getFormattedTemp(state.celsiusTemperature);
  celsiusLink.classList.add('active');
  fahrenheitLink.classList.remove('active');
}

function showFahrenheit() {
  state.currentUnit = 'F';
  temperatureElement.innerHTML = getFormattedTemp(state.celsiusTemperature);
  celsiusLink.classList.remove('active');
  fahrenheitLink.classList.add('active');
}

// eslint-disable-next-line import/prefer-default-export
export function startApp(city) {
  cityElement.innerHTML = city;
  getWeatherByCityName(city);
  formatDate();
  locationElement.addEventListener('click', getCurrentPosition);
  searchFormElement.addEventListener('submit', searchCity);
  celsiusLink.addEventListener('click', showCelsius);
  fahrenheitLink.addEventListener('click', showFahrenheit);
}
